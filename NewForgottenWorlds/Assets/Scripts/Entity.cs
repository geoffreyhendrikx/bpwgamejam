﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [SerializeField]
    private Sprite[] images;
    private SpriteRenderer sr;
    private Vector2 minPos = new Vector2(0, -2.5f);
    private Vector2 maxPos = new Vector2(5, 2.5f);
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        sr.color = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
        StartCoroutine(changeColor());

        randomSprite();
        RandomPosition();
    }
    IEnumerator changeColor()
    {
        yield return new WaitForSeconds(0.1f);
        sr.color = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
    }
    public void randomSprite()
    {
        sr.sprite = images[Random.Range(0, images.Length)];
    }
    public void RandomPosition() {
        Debug.Log("random pos");
        Vector2 tmp = new Vector2(Random.Range(minPos.x, maxPos.x), Random.Range( minPos.y, maxPos.y));
        transform.position = tmp;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
