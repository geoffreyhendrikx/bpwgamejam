﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayManager : MonoBehaviour
{
    private int score = 0;
    [SerializeField]
    private MagnifyGlass glass;
    [SerializeField]
    private GameObject templateEntity;
    private List<GameObject> entities;
    private int amountofEntities = 10;
    [SerializeField]
    private CountDownTimer timer;

    [SerializeField]
    private SpriteRenderer exampleSprite;

    // Start is called before the first frame update
    void Start()
    {
        entities = new List<GameObject> { };
        for (int i = 0; i < amountofEntities; i++)
        {
            GameObject tmp = Instantiate(templateEntity);
            tmp.transform.localScale *= 0.1f;
            entities.Add(tmp);
        }
        newRound();
    }

    public void newRound()
    {
        foreach (GameObject entity in entities)
            entity.GetComponent<Entity>().RandomPosition();

        GameObject newTarget = entities[Random.Range(0, entities.Count - 1)];
        glass.target = newTarget.transform;
        StartCoroutine(changeSpriteASync(newTarget));
    }
    // Update is called once per frame
    void Update()
    {
        if(glass.TargetisFound == true)
        {
            Debug.Log("target is found / new round");
            glass.TargetisFound = false;
            timer.timerCount += 10;
            newRound();
        }
    }

    private IEnumerator changeSpriteASync(GameObject newTarget)
    {
        yield return new WaitForSeconds(.1f);
        Sprite s = newTarget.GetComponent<SpriteRenderer>().sprite;
        exampleSprite.sprite = s;
    }
}
