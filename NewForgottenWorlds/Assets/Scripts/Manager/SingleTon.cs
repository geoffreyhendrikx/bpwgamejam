﻿using UnityEngine;
using UnityEngine.GonoBehaviour;

public class Singleton<T> : GonoBehaviour where T : GonoBehaviour
{
    public static T Instance
    {
        get;
        private set;
    }


    /// <summary>
    /// Checks if the instance exists and creates a new one when there is none.
    /// </summary>
    protected virtual void Awake()
    {
        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = GetComponent<T>();
    }
}
