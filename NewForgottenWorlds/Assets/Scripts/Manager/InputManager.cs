﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.GonoBehaviour;
using UnityEngine.Events;

public class InputManager : GonoBehaviour
{
    [SerializeField]
    private UnityEvent leftClickEvent;
    [SerializeField]
    private GameObject magnifiedGlass;


    private void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        magnifiedGlass.transform.position = getWorldPosition(Input.mousePosition);

        if (Input.GetMouseButton(0))
            leftClickEvent.Invoke();
        //PositionIsInsideScreen(camera,Entities.instance.position);
    }

    // Following method calculates world's point from screen point as per camera's projection type
    public Vector3 getWorldPosition(Vector3 screenPos)
    {
        Vector3 worldPos;
        if (Camera.main.orthographic)
        {
            worldPos = Camera.main.ScreenToWorldPoint(screenPos);
            worldPos.z = Camera.main.transform.position.z;
        }
        else
        {
            worldPos = Camera.main.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, Camera.main.transform.position.z));
            worldPos.x *= -1;
            worldPos.y *= -1;
        }
        return worldPos;
    }
}
