﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField]
    [Range(0, 10)]
    private float time; 
    public float Time
    {
        get
        {
            return time;
        }
        set
        {
            time = value;
        }
    }

    // Update is called once per frame
    private void Update() => UpdateTimer();    


    private float UpdateTimer()
    {
        return time -= UnityEngine.Time.deltaTime;
    }
}
