﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UnityEngine.GonoBehaviour
{
    /// <summary>
    /// Copyrighted © Geoffrey Hendrikx 2019
    /// This class contains MonoBehaviour Extensions.
    /// 
    /// All Rights reserved
    /// </summary>
    public class GonoBehaviour : MonoBehaviour
    {
        #region Trigger section
        protected UnityAction<Collider> onTriggerEnterEvent;
        protected UnityAction<Collider> onTriggerStayEvent;
        protected UnityAction<Collider> onTriggerExitEvent;
        #endregion

        #region Collsion section
        protected UnityAction<Collision> onCollisionEnterEvent;
        protected UnityAction<Collision> onCollisionStayEvent;
        protected UnityAction<Collision> onCollisionExitEvent;
        #endregion

        protected UnityAction onDestroyedEvent;
        protected UnityAction onEnableAction;

        /// <summary>
        /// Calculate the total bounds of this object.
        /// </summary>
        protected Bounds TotalBounds
        {
            get
            {
                Quaternion PrevRot = transform.rotation;
                transform.rotation = Quaternion.identity;

                Bounds Bounds = new Bounds(transform.position, Vector3.zero);

                foreach (Renderer Renderer in GetComponentsInChildren<Renderer>())
                {
                    Bounds.Encapsulate(Renderer.bounds);
                }

                Bounds.center = Bounds.center - transform.position;

                transform.rotation = PrevRot;

                return Bounds;
            }
        }

        /// <summary>
        /// Gets all the childeren;
        /// </summary>
        protected List<GameObject> GetChildren()
        {
            List<GameObject> children = new List<GameObject>();

            int childCount = transform.childCount;

            for (int i = 0; i < childCount; i++)
                children.Add(transform.GetChild(i).gameObject);

            return children;
        }

        /// <summary>
        /// Set Children active or inactive
        /// </summary>
        protected void SetChildrenActive(bool active)
        {
            int childCount = transform.childCount;

            for (int i = 0; i < childCount; i++)
                transform.GetChild(i).gameObject.SetActive(active);
        }

        /// <summary>
        /// Sets the position of a transform's children to zero.
        /// </summary>
        /// <param name="transform">Parent transform.</param>
        /// <param name="recursive">Also reset ancestor positions?</param>
        protected void ResetChildPositions(Transform transform, bool recursive = false)
        {
            foreach (Transform child in transform)
            {
                child.position = Vector3.zero;

                if (recursive)
                    ResetChildPositions(child.transform, recursive);
            }
        }

        /// <summary>
        /// Sets the layer of the transform's children.
        /// </summary>
        /// <param name="transform">Parent transform.</param>
        /// <param name="layerName">Name of layer.</param>
        /// <param name="recursive">Make it a Recursive function</param>
        protected void SetChildLayers(Transform transform, string layerName, bool recursive = false)
        {
            int layer = LayerMask.NameToLayer(layerName);
            SetChildLayersHelper(transform, layer, recursive);
        }

        /// <summary>
        /// Sets the layer of the transform's children.
        /// </summary>
        /// <param name="transform">Parent transform.</param>
        /// <param name="layerName">interger of layer.</param>
        /// <param name="recursive">Make it a Recursive function?</param>
        protected void SetChildLayersHelper(Transform transform, int layer, bool recursive)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.layer = layer;

                if (recursive)
                    SetChildLayersHelper(child, layer, recursive);
            }
        }

        /// <summary>
        /// Clear Children
        /// </summary>
        protected void ClearChildren()
        {
            int childCount = transform.childCount;

            for (int i = 0; i < childCount; i++)
                Destroy(transform.GetChild(i));
        }

        /// <summary>
        /// Set the collision Active or Inactive
        /// </summary>
        /// <param name="active"></param>
        protected void SetCollisionActive(bool active)
        {
            Collider[] components = GetComponentsInChildren<Collider>(true);

            for (int i = 0; i < components.Length; i++)
                components[i].enabled = active;
        }

        /// <summary>
        /// Resetting the Transform. 
        /// </summary>
        protected void ResetTransformn(Transform transform, bool includingChilds)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.zero;

            if (includingChilds)
                for (int i = 0; i < transform.childCount; i++)
                {
                    Transform child = transform.GetChild(i);

                    child.localPosition = Vector3.zero;
                    child.localRotation = Quaternion.identity;
                    child.localScale = Vector3.one;
                }
        }

        /// <summary>
        /// Gets a component attached to the given game object.
        /// If one isn't found, a new one is attached and returned.
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        /// <returns>Previously or newly attached component.</returns>
        protected T GetOrAddComponent<T>(GameObject gameObject) where T : Component
        {
            return gameObject.GetComponent<T>() ?? gameObject.AddComponent<T>();
        }

        /// <summary>
        /// Checks whether a game object has a component of type T attached.
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        /// <returns>True when component is attached.</returns>
        protected bool HasComponent<T>(GameObject gameObject) where T : Component
        {
            return gameObject.GetComponent<T>() != null;
        }

        /// <summary>
        /// These functions will be executed if the gameobject is in the OnTrigger function.
        /// </summary>
        #region OnTrigger section
        protected virtual void OnTriggerEnter(Collider other)
        {
            onTriggerEnterEvent?.Invoke(other);
        }

        protected virtual void OnTriggerStay(Collider other)
        {
            onTriggerStayEvent?.Invoke(other);
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            onTriggerExitEvent?.Invoke(other);
        }
        #endregion

        /// <summary>
        /// These functions will be executed if the gameobject has collision.
        /// </summary>
        #region OnCollsion Section
        protected virtual void OnCollisionEnter(Collision other)
        {
            onCollisionEnterEvent?.Invoke(other);
        }

        protected virtual void OnCollisionStay(Collision other)
        {
            onCollisionStayEvent?.Invoke(other);
        }

        protected virtual void OnCollisionExit(Collision other)
        {
            onCollisionExitEvent?.Invoke(other);
        }
        #endregion

        /// <summary>
        /// These functions will be executed if the gameobject is Destroyed.
        /// </summary>
        #region Destroy Section
        /// <summary>
        /// Destroy the GameObject.
        /// </summary>
        protected virtual void DestroyGameObject()
        {
            onDestroyedEvent?.Invoke();

            Destroy(this);
        }

        /// <summary>
        /// Destroy in a certain time a GameObject.
        /// </summary>
        /// <param name="time">time in seconds</param>
        protected void OnDestroyInCertainTime(float time)
        {
            onDestroyedEvent?.Invoke();

            Destroy(this, time);
        }
        #endregion

        /// <summary>
        /// OnEnable Action Invoked 
        /// </summary>
        protected virtual void OnEnable()
        {
            onEnableAction?.Invoke();
        }

        /// <summary>
        /// liniare interpolation between 2 objects
        /// </summary>
        /// <param name="transformObject">This transform</param>
        /// <param name="targetTransform">Target</param>
        /// <param name="timeStamp">Time stamp for the lerp</param>
        protected IEnumerator LerpingObjects(Transform transformObject, Vector3 targetPosition, float overTime, UnityAction callback)
        {
            float startTime = Time.time;
            Vector3 currentPosition = transformObject.position;

            while (Time.time < (startTime + overTime))
            {
                transformObject.position = Vector3.Lerp(currentPosition, targetPosition, (Time.time - startTime) / overTime);
                yield return null;
            }

            callback.Invoke();
        }

        /// <summary>
        /// Spherical linear interpolation between 2 objects
        /// </summary>
        /// <param name="transformObject">This transform</param>
        /// <param name="targetTransform">Target</param>
        /// <param name="timeStamp">Time stamp for the lerp</param>
        protected IEnumerator SlerpingObjects(Transform transformObject, Transform targetRotation, float overTime, UnityAction callback)
        {
            float startTime = Time.time;
            Vector3 currentPosition = transformObject.position;

            while (Time.time < (startTime + overTime))
            {
                transformObject.rotation = Quaternion.Slerp(transformObject.rotation, targetRotation.rotation, (Time.time - startTime) / overTime);
                yield return null;
            }

            callback.Invoke();
        }

        /// <summary>
        /// if position is inside the screen the function will return true otherwise false.
        /// </summary>
        /// <param name="position">Position in Vector 2</param>
        protected bool PositionIsInsideScreen(Vector2 position)
        {
            Rect screen = new Rect(0, 0, Screen.width, Screen.height);

            if (screen.Contains(position))
                return true;

            return false;
        }

        /// <summary>
        /// if position is inside the screen the function will return true otherwise false.
        /// </summary>
        /// <param name="position">Position in Vector 2</param>
        protected bool PositionIsInsideScreen(Camera camera, Vector2 target)
        {
            Rect screen = camera.rect;

            if (screen.Contains(target))
                return true;

            return false;
        }


        /// <summary>
        /// Moving the rigidbody with the x and z axis.
        /// </summary>
        /// <param name="rb">Rigidbody of the object</param>
        /// <param name="inputX">X Input</param>
        /// <param name="inputZ">Z Input</param>
        /// <param name="speed">Speed of the Object</param>
        protected void MovePositionRigidbody(Rigidbody rb, float inputX, float inputZ, float speed)
        {
            Vector3 forwardMovement = (rb.transform.forward * inputZ * speed * Time.deltaTime);
            Vector3 sidewaysMovement = (rb.transform.right * inputX * speed * Time.deltaTime);

            rb.MovePosition(rb.transform.position + (forwardMovement) + (sidewaysMovement));
        }

        /// <summary>
        /// Moving the rigidbody with force in the x and z axis.
        /// </summary>
        /// <param name="rb">Rigidbody of the object</param>
        /// <param name="inputX">X Input</param>
        /// <param name="inputZ">Z Input</param>
        /// <param name="speed">Speed of the Object</param>
        protected void AddForceRigidbody(Rigidbody rb, float inputX, float inputZ, float speed)
        {
            Vector3 forwardMovement = (rb.transform.forward * inputZ * speed * Time.deltaTime);
            Vector3 sidewaysMovement = (rb.transform.right * inputX * speed * Time.deltaTime);

            rb.AddForce(rb.transform.position + (forwardMovement) + (sidewaysMovement));
        }

        /// <summary>
        /// Finds the position closest to the given one.
        /// </summary>
        /// <param name="position">World position.</param>
        /// <param name="otherPositions">Other world positions.</param>
        /// <returns>Closest position.</returns>
        protected Vector3 GetClosest(Vector3 position, List<Vector3> otherPositions)
        {
            Vector3 closest = Vector3.zero;
            float shortestDistance = Mathf.Infinity;

            foreach (Vector3 otherPosition in otherPositions)
            {
                float distance = (position - otherPosition).sqrMagnitude;

                if (distance < shortestDistance)
                {
                    closest = otherPosition;
                    shortestDistance = distance;
                }
            }

            return closest;
        }

        /// <summary>
        /// Finds the position farest to the given one.
        /// </summary>
        /// <param name="position">World position.</param>
        /// <param name="otherPositions">Other world positions.</param>
        /// <returns>Closest position.</returns>
        protected Vector3 GetFarest(Vector3 position, List<Vector3> otherPositions)
        {
            Vector3 farestVector = Vector3.zero;

            float farestDistance = 0;

            for (int i = 0; i < otherPositions.Count; i++)
            {
                float distance = (position - otherPositions[i]).sqrMagnitude;
                if (distance > farestDistance)
                {
                    farestVector = otherPositions[i];
                    farestDistance = distance;
                }
            }

            return farestVector;
        }

        /// <summary>
        /// Get the avarage position of an array
        /// </summary>
        /// <returns> Avarage position</returns>
        protected Vector3 GetAvarage(Vector3[] positions)
        {
            Vector3 temp = Vector3.zero;

            for (int i = 0; i < positions.Length; i++)
                temp += positions[i];

            temp /= (positions.Length - 1);

            return temp;
        }

        /// <summary>
        /// Multiplying quaternions.
        /// Multiplying quaternions is like a adding effect with Quaternions.
        /// </summary>
        /// <returns>Multiplied Quaternions</returns>
        protected Quaternion MultiplyQuaternion(Quaternion baseQuaternion, Quaternion newQuaternion)
        {
            return (baseQuaternion * newQuaternion);
        }

        /// <summary>
        /// Gives a random value between -2 and 2.
        /// </summary>
        /// <param name="baseQuaternion">default transform.rotation</param>
        /// <param name="axis">X-Y-Z random offset </param>
        protected Quaternion GetRandomOffset(Quaternion baseQuaternion, string axis)
        {
            float r = Random.Range(-2, 2);

            Quaternion newQuaternion = new Quaternion(baseQuaternion.x, baseQuaternion.y, baseQuaternion.z, baseQuaternion.w);

            switch (axis)
            {
                case "X":
                    newQuaternion.x += r;
                    break;
                case "Y":
                    newQuaternion.y += r;
                    break;
                case "Z":
                    newQuaternion.z += r;
                    break;
                default:
                    newQuaternion = new Quaternion(baseQuaternion.x + r, baseQuaternion.y + r, baseQuaternion.z + r, baseQuaternion.w);
                    break;
            }

            return newQuaternion;
        }

    }
}